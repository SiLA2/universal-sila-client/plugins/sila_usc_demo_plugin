const fs = require('fs');
const dependencies = require('./package.json').dependencies;
const commonDeps = Object.keys(require("@sila2/universal-client-front/plugin/remote-component.config").resolve);

const commonDependencies = Object.keys(dependencies).filter(dep => {
    return dep !== '@sila2/universal-client-front' && commonDeps.includes(dep);
});

const includedDependencies = Object.keys(dependencies).filter(dep => {
    return dep !== '@sila2/universal-client-front' && !commonDeps.includes(dep);
});

const header = ((includedDependencies.length) ?
    ("// Bundled dependencies \n" + includedDependencies.map(dep => "//  " + dep).join("\n")) :
    ("// No dependency to bundle")) + "\n\n"
const begin = '// Common dependencies \n' +
    'module.exports = {\n' +
    '  resolve: {\n';
const middle = commonDependencies.map(dep => "    \"" + dep + "\": require(\"" + dep + "\")").join(",\n") + "\n"
const end =
    '  }\n' +
    '};\n'

const file = header + begin + middle + end;

fs.writeFileSync('./remote-component.config.js', file, {encoding:'utf8',flag:'w'})