import {HelloPage} from "../pages/helloPage";


export const PluginRootRoute = {
    routes: [
        {
            path : 'hello',
            element : <HelloPage/>,
        }
    ]
};
