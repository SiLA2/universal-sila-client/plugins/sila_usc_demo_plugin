import {HelloServerPage} from "../pages/helloServerPage";


export const PluginRootServerRoute = {
    routes: [
        {
            path : 'helloServer',
            element : <HelloServerPage/>,
        }
    ]
};
