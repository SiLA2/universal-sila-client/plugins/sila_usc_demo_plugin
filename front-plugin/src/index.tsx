/**
 * Entrypoint of the Remote Component.
 */
import PluginImplementation from "@sila2/universal-client-front/plugin/PluginImplementation";
import {PluginBinaryTypeForm} from "./components/PluginBinaryTypeForm";
import {PluginServerCallRequestBody} from "./components/PluginServerCallRequestBody";
import {HelloComponent} from "./components/HelloComponent";
import {PluginSidebarNavItem} from "./sidebar/PluginSidebarNavItem";
import {PluginSidebarServerItem} from "./sidebar/PluginSidebarServerItem";
import {PluginRootServerRoute} from "./routes/PluginRootServerRoute";
import {PluginRootRoute} from "./routes/PluginRootRoute";

const Index: PluginImplementation = {
    /*PluginAnyTypeForm: HelloComponent('PluginAnyTypeForm'),
    PluginBooleanTypeForm: HelloComponent('PluginBooleanTypeForm'),
    PluginDateTypeForm: HelloComponent('PluginDateTypeForm'),
    PluginIntegerTypeForm: HelloComponent('PluginIntegerTypeForm'),
    PluginRealTypeForm: HelloComponent('PluginRealTypeForm'),
    PluginStringTypeForm: HelloComponent('PluginStringTypeForm'),
    PluginTimeTypeForm: HelloComponent('PluginTimeTypeForm'),
    PluginTimestampTypeForm: HelloComponent('PluginTimestampTypeForm'),
    PluginConstraintHelpText: HelloComponent('PluginConstraintHelpText'),
    PluginNumberUnitConstraint: HelloComponent('PluginNumberUnitConstraint'),
    PluginBasicTypeForm: HelloComponent('PluginBasicTypeForm'),
    PluginDataTypeTypeForm: HelloComponent('PluginDataTypeTypeForm'),
    PluginDataTypeIdentifierTypeForm: HelloComponent('PluginDataTypeIdentifierTypeForm'),
    PluginListTypeForm: HelloComponent('PluginListTypeForm'),
    PluginStructureTypeForm: HelloComponent('PluginStructureTypeForm'),
    PluginDataTypeFormRoot: HelloComponent('PluginDataTypeFormRoot'),
    PluginDataTypeFormNode: HelloComponent('PluginDataTypeFormNode'),
    PluginServerCallRequestMetadatas: HelloComponent('PluginServerCallRequestMetadatas'),
    PluginServerCallRequestParameter: HelloComponent('PluginServerCallRequestParameter'),
    PluginCallResponse: HelloComponent('PluginCallResponse'),
    PluginCallResponseError: HelloComponent('PluginCallResponseError'),
    PluginCallResponseExecInfo: HelloComponent('PluginCallResponseExecInfo'),
    PluginCallResponseIntermediateResponses: HelloComponent('PluginCallResponseIntermediateResponses'),
    PluginCallResponseMetadatas: HelloComponent('PluginCallResponseMetadatas'),
    PluginCallResponseObservablePropertyValues: HelloComponent('PluginCallResponseObservablePropertyValues'),
    PluginCallResponseParameters: HelloComponent('PluginCallResponseParameters'),
    PluginCallResponseResponses: HelloComponent('PluginCallResponseResponses'),
    PluginCallResponses: HelloComponent('PluginCallResponses'),
    PluginServerCallComponent: HelloComponent('PluginServerCallComponent'),
    PluginServerCalls: HelloComponent('PluginServerCalls'),
    PluginServerCardSmallBody: HelloComponent('PluginServerCardSmallBody'),*/
    PluginServerCardBody: HelloComponent('PluginServerCardBody'),
    PluginBinaryTypeForm,
    PluginServerCallRequestBody,
    PluginSidebarNavItem,
    PluginSidebarServerItem,
    PluginRootRoute,
    PluginRootServerRoute
}

export { Index };
