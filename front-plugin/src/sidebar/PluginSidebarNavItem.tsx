import {Cottage} from "@mui/icons-material";
import {NavItem} from "@sila2/universal-client-front/components/sidebar/NavItem";

export function PluginSidebarNavItem(): JSX.Element {
    return (
        <>
            <NavItem key={'/hello'} path={'/hello'} title={'Hello'} icon={<Cottage/>}/>
        </>
    )
}
