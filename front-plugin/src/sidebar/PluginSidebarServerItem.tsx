import {MapsHomeWork} from "@mui/icons-material";
import {ServerItem} from "@sila2/universal-client-front/components/sidebar/ServerItem";
import {UscServer} from "@sila2/universal-client-front/generated/silaModels";

export interface PluginSidebarServerItemProps {
    currentServer: UscServer;
}
export function PluginSidebarServerItem(props: PluginSidebarServerItemProps): JSX.Element {
    return (
        <>
            <ServerItem
                id="sidebar-current-server-hello-server"
                pathEnd="helloServer"
                titleEnd="HelloServer"
                icon={<MapsHomeWork/>}
                serverName={props.currentServer.name}
                serverUuid={props.currentServer.serverUuid}
            />
        </>
    )
}
