import React from "react";
import {Typography} from "@mui/material";

export const HelloComponent = (componentName: string) => (props: any): JSX.Element => (
        <Typography>Hello from {componentName}</Typography>
);
