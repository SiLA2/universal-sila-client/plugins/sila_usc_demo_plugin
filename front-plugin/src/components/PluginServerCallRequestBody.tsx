import React, {useMemo} from "react";
import pako from 'pako';
import {Box, Button, Modal, SxProps, Theme, Typography} from "@mui/material";
import QrCodeIcon from '@mui/icons-material/QrCode';
// @ts-ignore
import QrReader from 'react-qr-scanner'
import {useDispatch} from "react-redux";
import {callInputUpdate} from "@sila2/universal-client-front/store/front/server/callInput/actions";
import { QRCode } from 'react-qrcode-logo';
import {base64ToBytes, bytesToBase64} from "byte-base64";
import {
    metadatasStateToGrpcJson,
    parametersStateToGrpcJson
} from "@sila2/universal-client-front/components/server/call/dataTypeForm/stateToGrpcJson";
import {
    dataTypeToState,
    getCommandResponseState
} from "@sila2/universal-client-front/components/server/call/dataTypeForm/silaToState";
import {
    PluginServerCallRequestBodyProps,
} from "@sila2/universal-client-front/plugin/PluginImplementation";
import {SiLATypeValueMap} from "@sila2/universal-client-front/store/front/server/callInput/types";
import {BasicValueType} from "@sila2/universal-client-front/components/server/call/dataTypeForm/type/Basic";

// qr code can be decoded online by
//  parsing the qr code https://zxing.org/w/decode.jspx
//  then decompressing gzip the raw text data https://codebeautify.org/gzip-decompress-online
export const PluginServerCallRequestBody = (props: PluginServerCallRequestBodyProps): JSX.Element => {
    const [open, setOpen] = React.useState<'no' | 'qr' | 'scan'>('no');
    const handleOpen = () => setOpen('qr');
    const handleOpenScan = () => setOpen('scan');
    const handleClose = () => setOpen('no');
    const dispatch = useDispatch();

    const qrcode = useMemo(() => {
        if (open !== 'qr') {
            return null;
        }
        const jsonValue = JSON.stringify({
            parameters: (props.parametersDefinition && props.parameters) ? parametersStateToGrpcJson(props.dataTypeDefinitions, props.parametersDefinition, props.parameters) : undefined,
            metadatas: props.metadatasDefinition && props.metadatas ? metadatasStateToGrpcJson(props.metadatasDefinition, props.metadatas) : undefined
        });
        const compressed = bytesToBase64(pako.deflate(jsonValue));
        console.log(compressed.length);
        if (compressed.length >= 2000) {
            return <div>Parameters and or metadata does not fit in QR Code!</div>
        }
        return (
            <>
                <Typography variant={'h6'}>
                    Call Input Values
                </Typography>
                <div style={{background: 'white', padding: '1rem', height: 'fit-content', width: 'fit-content'}}>
                    <div style={{border: 'black 6px solid', lineHeight: 0}}>
                        <QRCode value={compressed} size={720} ecLevel={'L'} />
                    </div>
                </div>
            </>
        );
    }, [open]);

    const scan = useMemo(() => {
        if (open !== 'scan') {
            return null;
        }

        const handleScan = (data: {text: string}) => {
            if (data && data.text) {
                setOpen('no');
                const json = JSON.parse(pako.inflate(base64ToBytes(data.text), { to: 'string' }))
                if (json.parameters) {
                    const state = getCommandResponseState(props.dataTypeDefinitions, props.parametersDefinition, json.parameters);
                    Object.keys(state).forEach((p) => {
                        dispatch(callInputUpdate({
                            fullyQualifiedCommandId: props.fullyQualifiedCallId,
                            stateId: p,
                            updatedValue: (state as SiLATypeValueMap)[p] as BasicValueType,
                            formType: "PARAMETER",
                            serverId: props.serverId
                        }));
                    })
                }
                if (json.metadatas) {
                    const metadatasValue = Object.keys(json.metadatas).reduce((acc, key) => {
                        const identifiers = key.split('/');
                        const id = identifiers[5];
                        return {
                            ...acc,
                            [key]: json.metadatas[key]?.[id]
                        };
                    }, {});
                    props.metadatasDefinition.filter(m => (metadatasValue as SiLATypeValueMap)[m.fullyQualifiedIdentifier]).forEach((m) => {
                        const formValues = dataTypeToState([], m.dataType, m.relativeIdentifier, (metadatasValue as SiLATypeValueMap)[m.relativeIdentifier])
                        Object.keys(formValues).forEach((p) => {
                            dispatch(callInputUpdate({
                                fullyQualifiedCommandId: props.fullyQualifiedCallId,
                                stateId: p,
                                updatedValue: (formValues as SiLATypeValueMap)[p] as BasicValueType,
                                formType: "METADATA",
                                serverId: props.serverId
                            }));
                        })
                    });
                }
            }
        }
        const handleError = (err: Error) => {
            console.error(err)
        }

        return (
            <>
                <Typography variant={'h6'}>
                    Scan QR code
                </Typography>
                <QrReader
                    onError={handleError}
                    onScan={handleScan}
                    style={{ width: '100%' }}
                />
            </>
        );
    }, [open]);

    return (
        <div>
            <Button variant="contained" onClick={handleOpen}>
                <QrCodeIcon/>
            </Button>
            <Button variant="contained" onClick={handleOpenScan}>
                <QrCodeIcon/>
            </Button>
            <Modal
                open={open === 'qr'}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    {
                        qrcode
                    }
                </Box>
            </Modal>
            <Modal
                open={open === 'scan'}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    {
                        scan
                    }
                </Box>
            </Modal>
        </div>
    );
};

const style: SxProps<Theme> = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    width: '50%',
    transform: 'translate(-50%, -50%)',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};