import React from "react";
import {PluginBinaryTypeFormProps} from "@sila2/universal-client-front/plugin/PluginImplementation";

export const PluginBinaryTypeForm = (props: PluginBinaryTypeFormProps): JSX.Element => (
  <img src={`data:image/jpeg;base64,${props.value?.value || ''}`} alt={'Binary Image'}/>
);
