import {Box, Typography} from "@mui/material";
import {useSelector} from "react-redux";
import {getServerByIdSelector} from "@sila2/universal-client-front/store/sila/manager/server/selector";
import {AppState} from "@sila2/universal-client-front/store/rootReducer";
import {UscServer} from "@sila2/universal-client-front/generated/silaModels";
import {useParams} from "react-router-dom";

export interface HelloServerPageProps {}
export function HelloServerPage(props: HelloServerPageProps): JSX.Element {
    const {id} = useParams();
    const server: UscServer | undefined = useSelector((state: AppState) => getServerByIdSelector(state, id));

    return (
        <Box>
            <Typography variant="h1" component="h1" gutterBottom>
                {`Hello ${server?.name || 'unknown'} server!`}
            </Typography>
            <Typography>
                I'm a sample page from the USC Demo Plugin
            </Typography>
        </Box>
    )
}