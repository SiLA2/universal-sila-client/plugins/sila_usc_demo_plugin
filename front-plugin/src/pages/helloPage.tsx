import {Box, Typography} from "@mui/material";

export interface HelloPageProps {}
export function HelloPage(props: HelloPageProps): JSX.Element {
    return (
        <Box>
            <Typography variant="h1" component="h1" gutterBottom>
                Hello!
            </Typography>
            <Typography>
                I'm a sample page from the USC Demo Plugin
            </Typography>
        </Box>
    )
}