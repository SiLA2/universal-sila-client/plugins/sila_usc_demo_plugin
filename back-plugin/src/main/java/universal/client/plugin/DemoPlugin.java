package universal.client.plugin;

import com.silastandard.universalclient.plugin.PluginService;
import com.silastandard.universalclient.plugin.annotations.UscPlugin;
import com.silastandard.universalclient.plugin.events.lifecycle.PluginCloseEvent;
import com.silastandard.universalclient.plugin.events.lifecycle.PluginLoadEvent;
import com.silastandard.universalclient.plugin.events.lifecycle.PluginStartEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;


@Slf4j
@ComponentScan
@UscPlugin(name="Demo Plugin", version="0.9.0")
public class DemoPlugin implements PluginService {

    @Override
    public void onLoad(PluginLoadEvent event) {
        log.info("Loading demo plugin...");
    }

    @Override
    public void onStart(PluginStartEvent event) {
        log.info("Demo plugin started!");
    }

    @Override
    public void onStop(PluginCloseEvent event) {
        log.info("Demo plugin stopped.");
    }
}
