package universal.client.plugin;

import com.silastandard.universalclient.utils.UscJsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.cloudier.client.CloudierClientEndpoint;
import sila_java.library.cloudier.server.CloudierRequest;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.core.utils.GitRepositoryState;
import sila_java.library.manager.ServerManager;
import sila_java.library.server_base.identification.ServerInformation;
import sila_java.library.sila_base.EmptyClass;

import java.io.IOException;

@Slf4j
@RestController
public class RestTestController {

    public RestTestController() {
        log.info("Rest Test controller from plugin loaded successfully!");
    }

    @GetMapping("/test")
    String test() {
        // test the presence of class in different submodules
        SiLAFramework.String from = SiLAString.from("");
        String test = UscJsonUtils.toJson("Test");
        CloudierClientEndpoint.MessageListener t = silaServerMessage -> log.info("message");
        CloudierRequest<Object> request = new CloudierRequest<>("", new Object());
        try {
            GitRepositoryState gitRepositoryState = new GitRepositoryState();
            log.info("sila_java.library.core version " + gitRepositoryState.generateVersion());
        } catch (IOException ignored) {}
        log.info("There are {} servers in the manager", ServerManager.getInstance().getServers().size());
        ServerInformation serverInformation = new ServerInformation("Test", "Test server", "silastandard.com", "0.42");
        EmptyClass silaBase = new EmptyClass();
        SiLAFramework.String str = SiLAFramework.String.newBuilder().setValue("str").build();
        return "Test endpoint from plugin";
    }
}
