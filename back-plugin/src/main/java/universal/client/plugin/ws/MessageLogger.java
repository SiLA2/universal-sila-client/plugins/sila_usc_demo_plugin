package universal.client.plugin.ws;

import com.silastandard.universalclient.plugin.annotations.UscEventListener;
import com.silastandard.universalclient.plugin.events.websocket.WebsocketMessageEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import sila_java.library.manager.models.CallStarted;

@Slf4j
@Component
public class MessageLogger {
    public MessageLogger() {
        log.info("Demo plugin message logger instantiated");
    }

    @UscEventListener
    public void onWebsocketMessage(WebsocketMessageEvent event) {
        log.info("Outgoing WS message at {}", event.getDestination());
    }

    @EventListener(condition = "#event.endpoint.contains('/event/call/progress')")
    public void onCallProgressWebsocketMessage(WebsocketMessageEvent event) {
        if (event.isCancelled()) {
            log.info("Websocket message call/progress is cancelled");
            return;
        }
        Object message = event.getMessage();
        if (message instanceof CallStarted) {
            CallStarted callStarted = (CallStarted) message;
            log.info("New call to {} in progress ", callStarted.getSiLACall().getCallId());
        } else {
            event.setCancelled(true);
            log.warn("Unknown message sent for call/progress");
        }
    }
}
