package universal.client.plugin;

import com.silastandard.universalclient.plugin.annotations.UscEventListener;
import com.silastandard.universalclient.plugin.events.call.PersistedExecutionRequestEvent;
import com.silastandard.universalclient.plugin.events.call.RuntimeCallExecutionRequestEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Component
public class CallMetadataInjector {
    private static final Pattern ACCESS_TOKEN_PATTERN = Pattern.compile("\"AccessToken\":\\{\"value\":\"(.*)\"", Pattern.MULTILINE);

    public CallMetadataInjector() {
        log.info("Demo plugin call metadata injector instantiated");
    }

    @UscEventListener
    public void onPersistedExecutionRequest(PersistedExecutionRequestEvent event) {
        log.info("Persisted execution request");
        if (event.getRequest().getMetadatas().isPresent()) {
            final String metadataJson = event.getRequest().getMetadatas().get();
            final Matcher matcher = ACCESS_TOKEN_PATTERN.matcher(metadataJson);
            if (matcher.find() && matcher.groupCount() > 0) {
                // Replace access token by "Token-from-plugin"
                final String metadataJsonWithReplacedAccessToken = new StringBuilder(metadataJson)
                        .replace(matcher.start(1), matcher.end(1), "Token-from-plugin")
                        .toString();
                event.getRequest().setMetadatas(Optional.of(metadataJsonWithReplacedAccessToken));
            }
        }
    }

    @UscEventListener
    public void onRuntimeExecutionRequest(RuntimeCallExecutionRequestEvent event) {
        log.info("Runtime execution request");
    }
}
